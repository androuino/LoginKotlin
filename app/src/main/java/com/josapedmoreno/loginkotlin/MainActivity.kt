package com.josapedmoreno.loginkotlin

import android.app.Activity
import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.InputMethodManager.RESULT_UNCHANGED_SHOWN
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.josapedmoreno.loginkotlin._activities.BluetoothListActivity
import com.josapedmoreno.loginkotlin._bluetooth_connections.BluetoothConnections
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*






/**
 * Created by josapedmoreno on 2017/05/27.
 */
class MainActivity : AppCompatActivity(), CheckForNull {

    private val TAG = MainActivity::class.simpleName!!
    val ANDROID_DEVICE = "ANDROID_DEVICE"
    var mContext : Context = this@MainActivity

    // Intent request codes
    private val REQUEST_CONNECT_DEVICE_SECURE = 1
    private val REQUEST_CONNECT_DEVICE_INSECURE = 2
    private val REQUEST_ENABLE_BT = 3

    /**
     * Local Bluetooth adapter
     */
    private var mBluetoothAdapter: BluetoothAdapter? = null

    /**
     * Member object for the BluetoothConnections services
     */
    private var mBTConnection: BluetoothConnections? = null

    /**
     * Name of the connected device
     */
    private var mConnectedDeviceName: String? = null

    /**
     * String buffer for outgoing messages
     */
    private var mOutStringBuffer: StringBuffer? = null

    /**
     * BroadcastReceiver declaration
     */
    var mmReceiver = mReciever()
    var btAdapterFilter = IntentFilter()
    var arrayAdapter: ArrayAdapter<String>? = null

    private var blueberryAddress: String? = null
    private var isConnected: Boolean = true
    private var consumerIrManager: ConsumerIrManager? = null
    private var irCommand: IrCommand? = null
    private var IR_FREQUENCY = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        this.consumerIrManager = ConsumerIrManager.getSupportConsumerIrManager(applicationContext)
        this.consumerIrManager!!.carrierFrequencies

        blueberryAddress = "00:06:66:83:8A:AC"
        btAdapterFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED)
        btAdapterFilter.addAction(BluetoothDevice.ACTION_FOUND)
        btAdapterFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED)
        btAdapterFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED)

        val intent = Intent(this, BluetoothListActivity::class.java)
        arrayAdapter = ArrayAdapter<String>(this@MainActivity, R.layout.device_name)

        // Create an ArrayAdapter using the string array and a default spinner layout
        val adapter = ArrayAdapter.createFromResource(this, R.array.frequency, android.R.layout.simple_spinner_item)
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Apply the adapter to the spinner
        spnFrequency.setAdapter(adapter)

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        if (mBluetoothAdapter == null) {
            Toast.makeText(mContext, "Bluetooth Device not found!", Toast.LENGTH_SHORT).show()
            val builder = AlertDialog.Builder(this@MainActivity).setTitle("No Bluetooth Device Detected!")
                    ?.setMessage("This Application needs to close because\nyour device doens't have a bluetooth module or\ncheck if your bluetooth is properly working and come back again")
                    ?.setNegativeButton("Exit", object : DialogInterface.OnClickListener{
                        override fun onClick(p0: DialogInterface?, p1: Int) {
                            onDestroy()
                        }
                    })
                    ?.create()

            val alert = builder
            if (alert != null) {
                alert.show()
            }
        }

        // Initialize the BluetoothConnections to perform bluetooth connections
        mBTConnection = BluetoothConnections(mHandler)

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = StringBuffer("")

        btnFree?.setOnClickListener {
            if (!mBluetoothAdapter?.isEnabled!!) {
                Toast.makeText(mContext, "Bluetooth is not enabled!", Toast.LENGTH_SHORT).show()
                // We need to enable the Bluetooth, so we ask the user
                val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                // REQUEST_ENABLE_BT es un valor entero que vale 1
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
            } else {
                val builder = AlertDialog.Builder(this@MainActivity).setTitle("TYPE OF DEVICE")
                        ?.setPositiveButton("Other Device", object : DialogInterface.OnClickListener{
                            override fun onClick(dialog : DialogInterface?, p1: Int) {
                                if (mBluetoothAdapter?.isEnabled() as Boolean) {
                                    // Perform this
                                    val intent = Intent(this@MainActivity, BluetoothListActivity::class.java)
                                    startActivityForResult(intent, REQUEST_CONNECT_DEVICE_SECURE)

                                    if (mBTConnection != null) {
                                        // Only if the state is STATE_NONE, do we know that we haven't started already
                                        if (mBTConnection?.getState() === mBTConnection?.STATE_NONE) {
                                            // Start the Bluetooth send data services
                                            mBTConnection?.start()
                                        }
                                    }
                                } else if (!(mBluetoothAdapter?.isEnabled() as Boolean)) {
                                    val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
                                } else if (mBTConnection == null) {
                                    //sendCommand()
                                    Log.d(TAG, "Connected")
                                }
                            }
                        })
                        ?.setNegativeButton("Android Devices", object : DialogInterface.OnClickListener{
                            override fun onClick(p0: DialogInterface?, p1: Int) {
                                if (mBluetoothAdapter?.isEnabled() as Boolean) {
                                    // Launch the BluetoothListActivity to see devices and do scan
                                    val serverIntent = Intent(this@MainActivity, BluetoothListActivity::class.java)
                                    startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_INSECURE);

                                    if (mBTConnection != null) {
                                        // Only if the state is STATE_NONE, do we know that we haven't started already
                                        if (mBTConnection?.getState() === mBTConnection?.STATE_NONE) {
                                            // Start the Bluetooth send data services
                                            mBTConnection?.start()
                                        }
                                    }
                                } else if (!(mBluetoothAdapter?.isEnabled() as Boolean)) {
                                    val enableIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT)
                                } else if (mBTConnection == null) {
                                    //sendCommand()
                                    Log.d(TAG, "Connected")
                                }
                            }
                        })
                        ?.create()

                val alert = builder
                if (alert != null) {
                    alert.show()
                }
            }
        }

        btnBTDisconnect.setOnClickListener {
            btnFree.visibility = View.VISIBLE
            if (mBTConnection != null) {
                mBTConnection?.stop()
                textView.setText(R.string.kotlin_app)
                textView.setTextColor(Color.parseColor("#808080"))
                llForSendingData.visibility = View.GONE
                lvRecievedData.visibility = View.GONE
            }
            btnBTDisconnect.visibility = View.GONE
        }

        lvRecievedData.setOnItemClickListener { adapterView, view, i, l ->
            val info = (view as TextView).text.toString()
            var ir_codes = info.split(" ")
            for (i in 2..ir_codes.size - 1) {
                Log.d(TAG, ir_codes[i])
            }
            val resultList = getIntegerArray(ir_codes as ArrayList<String>) //strArrayList is a collection of Strings as you defined.
            Log.d(TAG, resultList.toString())
            Log.d(TAG, IR_FREQUENCY.toString())

            convertIntegers(resultList)
            string2dec.string2dec(convertIntegers(resultList), IR_FREQUENCY)
            irCommand = IrCommand(IR_FREQUENCY, convertIntegers(resultList))
            consumerIrManager!!.transmit(irCommand)
        }
    }

    fun convertIntegers(integers: List<Int>): IntArray {
        val ret = IntArray(integers.size)
        val iterator = integers.iterator()
        for (i in ret.indices) {
            ret[i] = iterator.next().toInt()
        }
        return ret
    }

    private fun getIntegerArray(stringArray: ArrayList<String>): ArrayList<Int> {
        val result = ArrayList<Int>()
        for (stringValue in stringArray) {
            try {
                //Convert String to Integer, and store it into integer array list.
                result.add(Integer.parseInt(stringValue))
            } catch (nfe: NumberFormatException) {
                //System.out.println("Could not parse " + nfe);
                Log.w("NumberFormat", "Parsing failed! $stringValue can not be an integer")
            }

        }
        return result
    }

    /**
     * The Handler that gets information back from the BluetoothConnections
     */
    val mHandler = object : Handler() {
        override fun handleMessage(msg: Message) {
            val activity = this@MainActivity
            when (msg.what) {
                Constants.MESSAGE_STATE_CHANGE -> when (msg.arg1) {
                    mBTConnection?.STATE_CONNECTED -> {
                        Log.d(TAG, "Connected")
                    }
                    mBTConnection?.STATE_CONNECTING -> {
                        Log.d(TAG, "Connecting")
                    }
                    mBTConnection?.STATE_LISTEN, mBTConnection?.STATE_NONE -> {
                        Log.d(TAG, "Listening")
                    }
                }
                Constants.MESSAGE_WRITE -> {
                    val writeBuf = msg.obj as ByteArray
                    // construct a string from the buffer
                    val writeMessage = String(writeBuf)
                }
                Constants.MESSAGE_READ -> {
                    val readBuf = msg.obj as ByteArray
                    // construct a string from the valid bytes in the buffer
                    val readMessage = String(readBuf, 0, msg.arg1)
                    var error: String? = null
                    if (readMessage.equals("$:06000F")) {
                        error = readMessage + " Error"
                        arrayAdapter?.add(error)
                    } else {
                        arrayAdapter?.add(readMessage)
                    }
                    lvRecievedData.adapter = arrayAdapter
                    lvRecievedData.post {
                        lvRecievedData.setSelection(arrayAdapter?.count!! - 1)
                    }
                }
                Constants.MESSAGE_DEVICE_NAME -> {
                    // save the connected device's name
                    mConnectedDeviceName = msg.data.getString(Constants.DEVICE_NAME)
                    if (null != activity) {
                        Toast.makeText(activity, "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT).show()
                        textView.setText(mConnectedDeviceName.toString())
                        textView.setTextColor(Color.rgb(0, 255, 0))
                        Log.d(TAG, "Connected")
                        // show everything that is hidden
                        btnFree.visibility = View.GONE
                        btnBTDisconnect.visibility = View.VISIBLE
                        llForSendingData.visibility = View.VISIBLE
                        lvRecievedData.visibility = View.VISIBLE
                        btnRead.visibility = View.VISIBLE
                        btnClear.visibility = View.VISIBLE
                        spnFrequency.visibility = View.VISIBLE
                        sendData() // send data once the connection is established
                    }
                }
                Constants.MESSAGE_TOAST -> if (null != activity) {
                    Toast.makeText(activity, msg.data.getString(Constants.TOAST), Toast.LENGTH_SHORT).show()
                    textView.setText(R.string.kotlin_app)
                    textView.setTextColor(Color.parseColor("#808080"))
                    llForSendingData.visibility = View.GONE
                    lvRecievedData.visibility = View.GONE
                    btnBTDisconnect.visibility = View.GONE
                    btnRead.visibility = View.GONE
                    btnClear.visibility = View.GONE
                    spnFrequency.visibility = View.GONE
                    btnFree.visibility = View.VISIBLE
                }
            }
        }
    }

    // check for empty text field
    override fun emptyText(text: String) {
        //var sendText : String = etSend.text.toString()
        /*
        if (sendText.equals("") || sendText.length >= 0 || sendText.isNullOrEmpty()) {
        }
        */

        if (TextUtils.isEmpty(text)) {
            etSend.setError(getString(R.string.error_on_empty))
            return
        } else {
            //var sendText : String = "$:22002408E00401503F5A446F000001"+text+"\r\n"
            var sendText : String = text+"\n"
            sendMessage(text+"\n")
        }
    }

    // function to send data
    fun sendData() {
        if (llForSendingData.visibility == View.VISIBLE) {
            btnSend.setOnClickListener {
                //sendMessage("1E002305E00401503F5A446F000002")
                emptyText(etSend.text.toString())
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(etSend.getWindowToken(),
                        RESULT_UNCHANGED_SHOWN)
            }
            btnRead.setOnClickListener {
                lvRecievedData.adapter = arrayAdapter
                var receiveText : String = "$:1E002305E00401503F5A446F000002\r\n"
                sendMessage(receiveText)
                //etSend.text.clear()
            }
            btnClear.setOnClickListener {
                lvRecievedData.adapter = null
                etSend.text.clear()
                arrayAdapter?.clear()
                arrayAdapter?.notifyDataSetChanged()
            }
            spnFrequency.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                    val item = parent.getItemAtPosition(position).toString()
                    IR_FREQUENCY = Integer.parseInt(item)
                }
                override fun onNothingSelected(parent: AdapterView<*>) {}
            }
        }
    }

    /**
     * Sends a message.

     * @param message A string of text to send.
     */
    fun sendMessage(message: String) {
        // Check that we're actually connected before trying anything
        if (mBTConnection?.getState() !== mBTConnection?.STATE_CONNECTED) {
            Toast.makeText(this@MainActivity, R.string.not_connected, Toast.LENGTH_SHORT).show()
            return
        }

        // Check that there's actually something to send
        if (message.length > 0) {
            // Get the message bytes and tell the BluetoothConnections to write
            val send = message.toByteArray()
            mBTConnection?.write(send)

            // Reset out string buffer to zero
            mOutStringBuffer?.setLength(0)
        }
    }

    /**
     * Establish connection with other divice

     * @param data   An [Intent] with [BluetoothListActivity.EXTRA_DEVICE_ADDRESS] extra.
     * *
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    fun connectDevice(data: Intent, secure: Boolean) {
        // Get the device MAC address
        val address = data.extras.getString(BluetoothListActivity.EXTRA_DEVICE_ADDRESS)
        // Get the BluetoothDevice object
        val device = mBluetoothAdapter?.getRemoteDevice(address)
        // Attempt to connect to the device
        mBTConnection?.connect(device as BluetoothDevice, secure)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_CONNECT_DEVICE_SECURE ->
                // When BluetoothListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        connectDevice(data, true)
                    }
                }
            REQUEST_CONNECT_DEVICE_INSECURE ->
                // When BluetoothListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        connectDevice(data, false)
                    }
                }
            REQUEST_ENABLE_BT ->
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled
                    //sendCommand()
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled")
                    Toast.makeText(this@MainActivity, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show()
                    //getActivity().finish();
                }
        }
    }

    private fun connectToExisting(device: BluetoothDevice) {
        //ConnectThread(device)
        Log.d(TAG, "Auto Connecting! -------------------------------------------------------------")
        mBTConnection!!.connect(device, true)
    }

    /**
     * BroadcastReceiver for checking if bluetooth in ON or OFF
     */
    inner class mReciever : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.d(TAG, "broadcastReceiver")
            val action = intent.action

            if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                val state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)
                when (state) {
                    BluetoothAdapter.STATE_ON -> {
                        // Launch the BluetoothListActivity to see devices and do scan
                        val mIntent = Intent(this@MainActivity, BluetoothListActivity::class.java)
                        startActivityForResult(mIntent, REQUEST_CONNECT_DEVICE_SECURE)
                    }

                    BluetoothAdapter.STATE_OFF ->
                        // if the user turned off the bluetooth, tops the connection from device
                        if (mBTConnection != null) {
                            mBTConnection!!.stop()
                        }
                }
            }

            //We don't want to reconnect to already connected device
            if (isConnected === false) {
                // When discovery finds a device
                if (BluetoothDevice.ACTION_FOUND == action) {
                    Log.d(TAG, "ACTION_FOUND")
                    // Get the BluetoothDevice object from the Intent
                    val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)

                    // Check if the found device is one we had comm with
                    if (device.address.equals(blueberryAddress))
                        connectToExisting(device)
                }
            }

            if (BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
                Log.d(TAG, "ACTION_ACL_CONNECTED")
                // Get the BluetoothDevice object from the Intent
                //val device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)

                // Check if the connected device is one we had comm with
                if (device.address.equals(blueberryAddress)) {
                    isConnected = true
                }
            } else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                Log.d(TAG, "ACTION_ACL_DISCONNECTED")
                val device = intent.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)

                if (device.address.equals(blueberryAddress)) {
                    isConnected = false
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return super.onCreateOptionsMenu(menu)
    }

    override fun onStart() {
        this@MainActivity.registerReceiver(mmReceiver, btAdapterFilter)
        super.onStart()
        // setup the sendCommand() method
        if (mBTConnection == null) {
            //sendCommand()
        }
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "Application Paused!")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "Application Resumed!")
    }

    override fun onDestroy() {
        this@MainActivity.unregisterReceiver(mmReceiver)
        super.onDestroy()
        if (mBTConnection != null) {
            mBTConnection?.stop()
            finish()
        }
    }
}