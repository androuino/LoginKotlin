package com.josapedmoreno.loginkotlin._activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.josapedmoreno.loginkotlin.R
import kotlinx.android.synthetic.main.activity_bluetooth.*

/**
 * Created by josapedmoreno on 2017/05/26.
 */

class FreeActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bluetooth)

        btnConnect.setOnClickListener {
            val intent = Intent(this, BluetoothListActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
