package com.josapedmoreno.loginkotlin._bluetooth_connections

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.josapedmoreno.loginkotlin.Constants
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.lang.reflect.InvocationTargetException
import java.util.*

/**
 * Created by josapedmoreno on 2017/05/27.
 */
class BluetoothConnections (handler: Handler) {

    open val STATE_CONNECTING: Int = 2 // now initiating an outgoing connection
    open val STATE_CONNECTED: Int = 3  // now connected to a remote device
    open val STATE_LISTEN: Int = 1     // now listening for incoming connections
    // setup TAG for this class
    private val TAG = BluetoothConnections::class.java.simpleName

    // Name for the SDP record when creating server socket
    val NAME_SECURE = "BluetoothConnectionSecure"
    val NAME_INSECURE = "BluetoothConnectionInsecure"

    // Unique UUID for this application
    val MY_UUID_SECURE = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")        // this UUID is for Android Device
    val MY_UUID_INSECURE = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66")      // this one is for Other Devices like HC-05, HC-06 modules and many more

    // Member fields and prototypes
    var mAdapter: BluetoothAdapter? = null
    var mHandler: Handler? = null
    var mSecureAcceptThread: AcceptThread? = null
    var mInsecureAcceptThread: AcceptThread? = null
    var mConnectThread: ConnectThread? = null
    var mConnectedThread: ConnectedThread? = null
    var mState: Int = 0

    // Constants that indicate the current connection state
    open val STATE_NONE: Int = 0       // we're doing nothing

    /**
     * Constructor. Prepares a new BluetoothChat session.

     * @param context The UI Activity Context
     * *
     * @param handler A Handler to send messages back to the UI Activity
     */
    init {
        mAdapter = BluetoothAdapter.getDefaultAdapter()
        mState = STATE_NONE
        mHandler = handler
    }

    /**
     * Set the current state
     *
     * @param state An integer defining the current connection state
     */
    @Synchronized fun setState(state: Int) {
        Log.d(TAG, "setState() $mState -> $state")
        mState = state

        // Give the new state to the Handler so the UI Activity can update
        mHandler?.obtainMessage(Constants.MESSAGE_STATE_CHANGE, state, -1)?.sendToTarget()
    }

    /**
     * Return the current connection state.
     */
    @Synchronized fun getState(): Int {
        return mState
    }

    /**
     * Start the Kotlin App. Specifically start AcceptThread to begin a
     * session in listening (server) mode. Called by the Activity onResume()
     *
     * this class will only listen to a single bluetooh connection/activity
     */
    @Synchronized fun start() {
        Log.d(TAG, "start")

        // Cancel any thread attempting to make a connection
        if (mConnectThread != null) {
            mConnectThread?.cancel()
            mConnectThread = null
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread?.cancel()
            mConnectedThread = null
        }

        setState(STATE_LISTEN)

        // Start the thread to listen on a BluetoothServerSocket
        if (mSecureAcceptThread == null) {
            mSecureAcceptThread = AcceptThread(true)
            mSecureAcceptThread?.start()
        }
        if (mInsecureAcceptThread == null) {
            mInsecureAcceptThread = AcceptThread(false)
            mInsecureAcceptThread?.start()
        }
    }

    /**
     * Start the ConnectThread to initiate a connection to a remote device.
     *
     * @param device The BluetoothDevice to connect
     *
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    @Synchronized fun connect(device: BluetoothDevice, secure: Boolean) {
        Log.d(TAG, "connect to: " + device)

        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectThread != null) {
                mConnectThread?.cancel()
                mConnectThread = null
            }
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread?.cancel()
            mConnectedThread = null
        }

        // Start the thread to connect with the given device
        mConnectThread = ConnectThread(device, secure)
        mConnectThread?.start()
        setState(STATE_CONNECTING)
    }

    /**
     * Start the ConnectedThread to begin managing a Bluetooth connection
     *
     * @param socket The BluetoothSocket on which the connection was made
     *
     * @param device The BluetoothDevice that has been connected
     */
    @Synchronized fun connected(socket: BluetoothSocket, device: BluetoothDevice, socketType: String) {
        Log.d(TAG, "connected, Socket Type:" + socketType)

        // Cancel the thread that completed the connection
        if (mConnectThread != null) {
            mConnectThread?.cancel()
            mConnectThread = null
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread?.cancel()
            mConnectedThread = null
        }

        // Cancel the accept thread because we only want to connect to one device
        if (mSecureAcceptThread != null) {
            mSecureAcceptThread?.cancel()
            mSecureAcceptThread = null
        }
        if (mInsecureAcceptThread != null) {
            mInsecureAcceptThread?.cancel()
            mInsecureAcceptThread = null
        }

        // Start the thread to manage the connection and perform transmissions
        mConnectedThread = ConnectedThread(socket, socketType)
        mConnectedThread?.start()

        // Send the name of the connected device back to the UI Activity
        val msg = mHandler?.obtainMessage(Constants.MESSAGE_DEVICE_NAME)
        val bundle = Bundle()
        bundle.putString(Constants.DEVICE_NAME, device.name)
        msg?.data = bundle
        mHandler?.sendMessage(msg)

        setState(STATE_CONNECTED)
    }

    /**
     * Stop all threads or if you want to disconnect the bluetooth connection
     */
    @Synchronized fun stop() {
        Log.d(TAG, "stop")

        if (mConnectThread != null) {
            mConnectThread?.cancel()
            mConnectThread = null
        }

        if (mConnectedThread != null) {
            mConnectedThread?.cancel()
            mConnectedThread = null
        }

        if (mSecureAcceptThread != null) {
            mSecureAcceptThread?.cancel()
            mSecureAcceptThread = null
        }

        if (mInsecureAcceptThread != null) {
            mInsecureAcceptThread?.cancel()
            mInsecureAcceptThread = null
        }
        setState(STATE_NONE)
    }

    /**
     * Write to the ConnectedThread in an unsynchronized manner
     *
     * @param out The bytes to write
     *
     * @see ConnectedThread.write
     */
    fun write(out: ByteArray) {
        // Create temporary object
        var r: ConnectedThread? = null
        // Synchronize a copy of the ConnectedThread
        synchronized(this) {
            if (mState != STATE_CONNECTED) return
            r = mConnectedThread!!
        }
        // Perform the write unsynchronized
        r?.write(out)
    }

    /**
     * Indicate that the connection attempt failed and notify the UI Activity.
     */
    private fun connectionFailed() {
        // Send a failure message back to the Activity
        val msg = mHandler?.obtainMessage(Constants.MESSAGE_TOAST)
        val bundle = Bundle()
        bundle.putString(Constants.TOAST, "Unable to connect device")
        msg?.data = bundle
        mHandler?.sendMessage(msg)

        // Start the service over to restart listening mode
        this@BluetoothConnections.start()
    }

    /**
     * Indicate that the connection was lost and notify the UI Activity.
     */
    private fun connectionLost() {
        // Send a failure message back to the Activity
        val msg = mHandler?.obtainMessage(Constants.MESSAGE_TOAST)
        val bundle = Bundle()
        bundle.putString(Constants.TOAST, "Device connection was lost")
        msg?.data = bundle
        mHandler?.sendMessage(msg)

        // Start the service over to restart listening mode
        this@BluetoothConnections.start()
    }

    /**
     * This thread runs while listening for incoming connections. It behaves
     * like a server-side client. It runs until a connection is accepted
     * (or until cancelled).
     */
    inner class AcceptThread(secure: Boolean) : Thread() {
        // The local server socket
        private val mmServerSocket: BluetoothServerSocket
        private val mSocketType: String

        init {
            var tmp: BluetoothServerSocket? = null
            mSocketType = if (secure) "Secure" else "Insecure"

            // Create a new listening server socket
            try {
                if (secure) {
                    tmp = mAdapter!!.listenUsingRfcommWithServiceRecord(NAME_SECURE, MY_UUID_SECURE)
                } else {
                    tmp = mAdapter!!.listenUsingInsecureRfcommWithServiceRecord(NAME_INSECURE, MY_UUID_INSECURE)
                }
            } catch (e: IOException) {
                Log.e(TAG, "Socket Type: " + mSocketType + "listen() failed", e)
            }

            mmServerSocket = tmp!!
        }

        override fun run() {
            Log.d(TAG, "Socket Type: " + mSocketType + "BEGIN mAcceptThread" + this)
            name = "AcceptThread" + mSocketType

            var socket: BluetoothSocket? = null

            // Listen to the server socket if we're not connected
            while (mState != STATE_CONNECTED) {
                try {
                    // This is a blocking call and will only return on a
                    // successful connection or an exception
                    socket = mmServerSocket.accept()
                } catch (e: IOException) {
                    Log.e(TAG, "Socket Type: " + mSocketType + "accept() failed", e)
                    //break
                    return
                }

                // If a connection was accepted
                if (socket != null) {
                    synchronized(this@BluetoothConnections) {
                        when (mState) {
                            STATE_LISTEN, STATE_CONNECTING ->
                                // Situation normal. Start the connected thread.
                                connected(socket!!, socket!!.remoteDevice,
                                        mSocketType)
                            STATE_NONE, STATE_CONNECTED ->
                                // Either not ready or already connected. Terminate new socket.
                                try {
                                    socket!!.close()
                                } catch (e: IOException) {
                                    Log.e(TAG, "Could not close unwanted socket", e)
                                }

                            else -> {
                            }
                        }
                    }
                }
            }
            Log.i(TAG, "END mAcceptThread, socket Type: " + mSocketType)

        }

        fun cancel() {
            Log.d(TAG, "Socket Type" + mSocketType + "cancel " + this)
            try {
                mmServerSocket.close()
            } catch (e: IOException) {
                Log.e(TAG, "Socket Type" + mSocketType + "close() of server failed", e)
            }

        }
    }

    /**
     * This thread runs while attempting to make an outgoing connection
     * with a device. It runs straight through; the connection either
     * succeeds or fails.
     */
    inner class ConnectThread(private val mmDevice: BluetoothDevice, secure: Boolean) : Thread() {
        private var mmSocket: BluetoothSocket? = null
        private val mSocketType: String

        init {
            var tmp: BluetoothSocket? = null
            mSocketType = if (secure) "Secure" else "Insecure"

            // Get a BluetoothSocket for a connection with the
            // given BluetoothDevice
            try {
                if (secure) {
                    tmp = mmDevice.createRfcommSocketToServiceRecord(MY_UUID_SECURE)
                } else {
                    tmp = mmDevice.createInsecureRfcommSocketToServiceRecord(MY_UUID_INSECURE)
                }
            } catch (e: IOException) {
                Log.e(TAG, "Socket Type: " + mSocketType + "create() failed", e)
            }

            mmSocket = tmp
        }

        override fun run() {
            Log.i(TAG, "BEGIN mConnectThread SocketType:" + mSocketType)
            name = "ConnectThread" + mSocketType

            // Always cancel discovery because it will slow down a connection
            mAdapter!!.cancelDiscovery()

            // Make a connection to the BluetoothSocket
            try {
                // This is a blocking call and will only return on a
                // successful connection or an exception
                mmSocket!!.connect()
            } catch (e: IOException) {
                // Close the socket
                try {
                    mmSocket = mmDevice.javaClass.getMethod("createRfCommSocket", *arrayOf<Class<*>>(Int::class.java)).invoke(mmDevice, 1) as BluetoothSocket
                    (mmSocket as BluetoothSocket).connect()
                } catch (e1: InvocationTargetException) {
                    e1.printStackTrace()
                } catch (e1: NoSuchMethodException) {
                    e1.printStackTrace()
                } catch (e1: IllegalAccessException) {
                    e1.printStackTrace()
                } catch (e1: IOException) {
                    try {
                        mmSocket!!.close()
                    } catch (e2: IOException) {
                        Log.e(TAG, "unable to close() $mSocketType socket during connection failure", e1)
                    }

                    connectionFailed()
                    return
                }

            }

            // Reset the ConnectThread because we're done
            synchronized(this@BluetoothConnections) {
                mConnectThread = null
            }

            // Start the connected thread
            connected(mmSocket!!, mmDevice, mSocketType)
        }

        fun cancel() {
            try {
                mmSocket!!.close()
            } catch (e: IOException) {
                Log.e(TAG, "close() of connect $mSocketType socket failed", e)
            }

        }
    }

    /**
     * This thread runs during a connection with a remote device.
     * It handles all incoming and outgoing transmissions.
     */
    inner class ConnectedThread(private val mmSocket: BluetoothSocket, socketType: String) : Thread() {
        val mmInStream: InputStream
        val mmOutStream: OutputStream
        //var mmBuffer: ByteArray? = null // mmBuffer store for the stream

        init {
            Log.d(TAG, "create ConnectedThread: " + socketType)
            var tmpIn: InputStream? = null
            var tmpOut: OutputStream? = null

            // Get the BluetoothSocket input and output streams
            try {
                tmpIn = mmSocket.inputStream
                tmpOut = mmSocket.outputStream
            } catch (e: IOException) {
                Log.e(TAG, "temp sockets not created", e)
            }

            mmInStream = tmpIn!!
            mmOutStream = tmpOut!!
        }

        override fun run() {
            Log.i(TAG, "BEGIN mConnectedThread")
            var buffer = ByteArray(1024)
            var bytes: Int = 0
            var arr_byte = ArrayList<Int>()

            // Keep listening to the InputStream while connected
            while (true) {
                try {
                    bytes = mmInStream.read()
                    if (bytes == 0x0A) {
                    } else if (bytes == 0x0D) {
                        buffer = ByteArray(arr_byte.size)
                        for (i in arr_byte.indices) {
                            //buffer[i] = arr_byte.get(i).toByte()
                            buffer[i] = arr_byte.get(i).toByte()
                        }
                        for (i in arr_byte.indices) {
                            System.out.println(arr_byte[i])
                        }
                        //Log.d(TAG, "bytes = "+arr_byte)
                        // Send the obtained bytes to the UI Activity
                        mHandler?.obtainMessage(Constants.MESSAGE_READ, buffer.size, -1, buffer)!!.sendToTarget()
                        arr_byte = ArrayList<Int>()
                    } else {
                        arr_byte.add(bytes)
                    }
                } catch (e: IOException) {
                    Log.e(TAG, "disconnected", e)
                    connectionLost()
                    // Start the service over to restart listening mode
                    this@BluetoothConnections.start()
                    break
                }

            }
        }

        /**
         * Write to the connected OutStream.

         * @param buffer The bytes to write
         */
        fun write(buffer: ByteArray) {
            try {
                mmOutStream.write(buffer)
                mmOutStream.flush()
                // Share the sent message back to the UI Activity
                mHandler!!.obtainMessage(Constants.MESSAGE_WRITE, -1, -1, buffer).sendToTarget()
            } catch (e: IOException) {
                Log.e(TAG, "Exception during write", e)
            }

        }

        fun cancel() {
            try {
                mmInStream.close()
                mmOutStream.close()
                mmSocket.close()
            } catch (e: IOException) {
                Log.e(TAG, "close() of connect socket failed", e)
            }

        }
    }
}