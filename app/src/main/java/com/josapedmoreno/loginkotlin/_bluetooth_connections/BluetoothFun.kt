package com.josapedmoreno.loginkotlin._bluetooth_connections

/**
 * Created by josapedmoreno on 2017/05/28.
 */
interface BluetoothFun {
    fun isBluetoothEnabled()
}