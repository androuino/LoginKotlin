package com.josapedmoreno.loginkotlin._dboperations

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

/**
 * Created by josapedmoreno on 2017/05/26.
 */

class DatabaseOperations(context: Context) : SQLiteOpenHelper(context, DatabaseData.DATABASE_NAME, null, DatabaseData.DATABASE_VERSION) {
    var createQuery = "create table " + DatabaseData.DATABASE_TABLE + "(" + DatabaseData.USER_NAME + " text, " + DatabaseData.USER_PASSWORD + " text);"

    companion object {

        private val TAG = DatabaseOperations::class.simpleName!!
    }

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        // create table
        sqLiteDatabase.execSQL(createQuery)
        Log.d(TAG, "DB table is created")
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {
        // check for the existence of the database table
        val query = "drop table if exist " + DatabaseData.DATABASE_TABLE
    }

    
}
