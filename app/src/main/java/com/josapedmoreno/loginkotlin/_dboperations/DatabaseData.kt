package com.josapedmoreno.loginkotlin._dboperations

/**
 * Created by josapedmoreno on 2017/05/26.
 */

interface DatabaseData {
    companion object {
        // database version
        val DATABASE_VERSION = 1
        // create the database name and table name
        val DATABASE_NAME = "user_info"
        val DATABASE_TABLE = "register_table"
        // create columns for table names of the database
        val USER_NAME = "user_name"
        val USER_PASSWORD = "user_password"
    }

    fun onSaveError(): Boolean?
}
