package com.josapedmoreno.loginkotlin;

import android.content.Context;
import android.os.Build;
import android.util.Log;

public abstract class ConsumerIrManager {

    private static final String TAG = "ConsumerIrManager";

    public boolean hasIrEmitter() {
        return false;
    }

    public void transmit(int carrierFrequency, int[] pattern) {
        transmit(carrierFrequency, pattern);
    }

    public void transmit(IrCommand command) {
        transmit(command.frequency, command.pattern);
    }

    public android.hardware.ConsumerIrManager.CarrierFrequencyRange[] getCarrierFrequencies() {
        return null;
    }

    public static ConsumerIrManager getSupportConsumerIrManager(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Log.d(TAG, "ConsumerIrManagerCompat");
            return new ConsumerIrManagerCompat(context);
        }

        ConsumerIrManager consumerIrManagerSamsung = ConsumerIrManagerSamsung.getSupportConsumerIrManager(context);

        if (consumerIrManagerSamsung != null) {
            Log.d(TAG, "ConsumerIrManagerSamsung");
            return consumerIrManagerSamsung;
        }

        return new ConsumerIrManager() {
        };
    }

}