package com.josapedmoreno.loginkotlin;

import android.os.Build;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by josapedmoreno on 1/31/16.
 */
public class string2dec {

    private static final String TAG = "string2dec";
    String[] data = {};

    /*
     * preforms some calculations on the codesets we have in order to make them work with certain models of phone.
     *
     * HTC devices need formula 1
     * Samsungs want formula 2
     *
     * Samsung Pre-4.4.3 want nothing, so just return the input data
     *
     */
    public static int[] string2dec(int[] irData, int frequency) {
        int formula = shouldEquationRun();

        try {
            //Should we run any computations on the irData?
            if (formula != 0) {
                for (int i = 0; i < irData.length; i++) {
                    if (formula == 1) {
                        irData[i] = (int) ((1000000L * Long.parseLong(String.valueOf(irData[i]))) / frequency);
                    } else if (formula == 2) {
                        //this is the samsung formula as per http://developer.samsung.com/android/technical-docs/Workaround-to-solve-issues-with-the-ConsumerIrManager-in-Android-version-lower-than-4-4-3-KitKat
                        irData[i] = (int) Math.ceil(irData[i] * 26.27272727272727);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return irData;
    }

    public static ArrayList<Integer> string2dec(ArrayList<Integer> irData, int frequency) {
        int formula = shouldEquationRun();

        try {
            //Should we run any computations on the irData?
            if (formula != 0) {
                for (int i = 0; i < irData.size(); i++) {
                    if (formula == 1) {
                        irData.set(i, (int) ((1000000L * Long.parseLong(String.valueOf(irData.get(i)))) / frequency));
                    } else if (formula == 2) {
                        //this is the samsung formula as per http://developer.samsung.com/android/technical-docs/Workaround-to-solve-issues-with-the-ConsumerIrManager-in-Android-version-lower-than-4-4-3-KitKat
                        irData.set(i, (int) Math.ceil(irData.get(i) * 26.27272727272727));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return irData;
    }

    /*
     * This method figures out if we should be running the equation in string2dec,
     * which is based on the type of device. Some need it to run in order to function, some need it NOT to run
     *
     * HTC needs it on (HTC One M8)
     * Samsung needs occasionally a special formula, depending on the version
     * Android 5.0+ need it on.
     * Other devices DO NOT need anything special.
     */
    private static int shouldEquationRun() {
        //Some notes on what Build.X will return
        //System.out.println(Build.MODEL); //One M8
        //System.out.println(Build.MANUFACTURER); //htc
        //System.out.println(Build.VERSION.SDK_INT); //19

        //Samsung's way of finding out if their OS is too new to work without a formula:
        //int lastIdx = Build.VERSION.RELEASE.lastIndexOf(".");
        //System.out.println(Build.VERSION.RELEASE.substring(lastIdx+1)); //4

        //handle HTC
        if (Build.MANUFACTURER.equalsIgnoreCase("HTC")) {
            Log.d(TAG, "HTC Handheld Systems");
            return 1;
        }
        //handle Lollipop (Android 5.0.1 == SDK 21) / beyond
        if (Build.VERSION.SDK_INT >= 21) {
            Log.d(TAG, "Android 5.0.1 Lollipop / SDK 21");
            return 1;
        }
        //handle Samsung PRE-Android 5
        if (Build.MANUFACTURER.equalsIgnoreCase("SAMSUNG")) {
            Log.d(TAG, "Samsung Handheld Systems");
            if (Build.VERSION.SDK_INT >= 19) {
                int lastIdx = Build.VERSION.RELEASE.lastIndexOf(".");
                int VERSION_MR = Integer.valueOf(Build.VERSION.RELEASE.substring(lastIdx + 1));
                if (VERSION_MR < 3) {
                    // Before version of Android 4.4.2
                    //Note: NO formula here, not even the other one
                    Log.d(TAG, "Samsung Android Version <= 4.4.2");
                    return 0;
                } else {
                    // Later version of Android 4.4.3
                    //run the special samsung formula here
                    Log.d(TAG, "Samsung Android Version >= 4.4.3");
                    return 2;
                }
            }
        }
        //if something else...
        return 0;
    }
}
